#!/bin/bash -l

# Detect Platform (PN = Platform Name, PV = Platform Version)

ARCH=$(uname -m)

case $(uname -s) in
        Darwin)
        echo "Running on $(sw_vers | cut -d: -f2 | xargs) ($(uname -n))"
        LABEL="mac$(sw_vers -productVersion | cut -d . -f 1,2 | tr -d .)"
        ;;

        Linux)
        if type -P lsb_release >/dev/null; then
                echo "Running on $(lsb_release --short -d) ($(uname -n))"

                PN=$(lsb_release --short -i)
                PV=$(lsb_release --short -r)

                case ${PN} in
                        *CentOS*) LABEL=${PN,,}${PV/\.*/} ;;
                        *Fedora*) LABEL=${PN,,}${PV/\.*/} ;;
                        *Gentoo*) LABEL=${PN,,}           ;;
                        *Ubuntu*) LABEL=${PN,,}${PV/\./}  ;;
                           *SLC*) LABEL=slc${PV%.*} ;;
                esac
        else
                echo "Running on $(uname -sr) ($(uname -n))"
                PN=$(uname -s)
                PV=$(uname -r)
        fi
        ;;
esac

LCG=/cvmfs/sft.cern.ch/lcg

# Setup CMake from LCG if needed

: ${CMAKE:=$(command -v cmake3 >/dev/null && which cmake3 || which cmake)}

if command -v ${CMAKE} >/dev/null; then
	: ${CMAKE_VERSION:=$(${CMAKE} --version | sed -ne '1s/cmake.*version //p')}
fi

if [[ -z ${CMAKE_VERSION} || ${CMAKE_VERSION} < 3 ]]; then
	CMAKE=$LCG/contrib/CMake/latest/$(uname -s)-$(uname -m)/bin/cmake
	if ! command -v ${CMAKE} >/dev/null; then
		echo Cannot setup CMake from LCG!
		exit 1
	fi
fi

# Compiler Name and Version

: ${CC:=cc} ${CXX:=c++}

CN=$(${CC} -v 2>&1 | grep -E -o 'clang|gcc|icc' | sed -ne 1p)
CV=$(${CC} -v 2>&1 | grep -o 'version \<.[\.0-9]*' | sed -ne '1s/version //p')

echo
echo Detected platform: ARCH=$ARCH, LABEL=$LABEL, COMPILER=${CN}-${CV}
echo

which ${CMAKE} && ${CMAKE} --version
echo
which ${CC} && ${CC} --version

env
