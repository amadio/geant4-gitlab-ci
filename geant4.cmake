set(CTEST_PROJECT_NAME "Geant4")

set(ENV{LANG} "C")
set(ENV{LC_ALL} "C")

set(CTEST_SOURCE_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/../geant4")
set(CTEST_BINARY_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/build")

if(NOT DEFINED CTEST_CONFIGURATION_TYPE)
  set(CTEST_CONFIGURATION_TYPE RelWithDebInfo)
endif()

if(DEFINED ENV{CMAKE_GENERATOR})
  set(CTEST_CMAKE_GENERATOR $ENV{CMAKE_GENERATOR})
else()
  execute_process(COMMAND ${CMAKE_COMMAND} --system-information
    OUTPUT_VARIABLE CMAKE_SYSTEM_INFORMATION ERROR_VARIABLE ERROR)
  if(ERROR)
    message(FATAL_ERROR "Could not detect default CMake generator")
  endif()
  string(REGEX REPLACE ".+CMAKE_GENERATOR \"([-0-9A-Za-z ]+)\".*$" "\\1"
    CTEST_CMAKE_GENERATOR "${CMAKE_SYSTEM_INFORMATION}")
endif()

if(DEFINED CTEST_SCRIPT_ARG)
  set(MODEL ${CTEST_SCRIPT_ARG})
else()
  set(MODEL Experimental)
endif()

if(DEFINED DEBUG_VARS)
  foreach(var ${DEBUG_VARS})
    message(STATUS "${var}='${${var}}'")
  endforeach()
endif()

if(EXISTS "${CTEST_BINARY_DIRECTORY}/CMakeCache.txt")
  ctest_empty_binary_directory("${CTEST_BINARY_DIRECTORY}")
endif()

set(CTEST_BUILD_OPTIONS 
  -DGEANT4_ENABLE_TESTING=ON
  -DGEANT4_INSTALL_DATA=ON
  ${CTEST_BUILD_OPTIONS}
  $ENV{CTEST_BUILD_OPTIONS})

ctest_start(${MODEL})
ctest_configure(OPTIONS "${CTEST_BUILD_OPTIONS}")
ctest_read_custom_files(${CTEST_BINARY_DIRECTORY})
ctest_build()
ctest_test()
